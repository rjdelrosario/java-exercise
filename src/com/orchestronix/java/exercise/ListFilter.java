package com.orchestronix.java.exercise;

public interface ListFilter {
	// Filters the given array for even numbers and returns a new array containing the even numbers
	int[] filterEven(int[] numbers);
	
	// Filters the given array for odd numbers and returns a new array container the odd numbers
	int[] filterOdd(int[] numbers);
	
	// Filters the given array based on a given Predicate
	int[] filterBy(int[] numbers, IntPredicate p);
	
	// Adds one to each number in the array and returns a new array whose elements are n+1
	int[] addOne(int[] numbers);
	
	// Returns the sum of the given numbers 	
	int sum(int[] numbers);
	
	// Transforms the numbers based on a given Transformer and returns a new array containing the transformed elements
	int[] transform(int[] numbers, IntTransformer t);
}
