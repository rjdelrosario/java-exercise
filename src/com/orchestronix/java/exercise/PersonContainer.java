package com.orchestronix.java.exercise;

import java.util.*;

public interface PersonContainer {
	// Adds a person to this container
	void add(Person p);
	
	// Clears this container
	void clear();
	
	// Removes a person from this container
	void remove(Person p);
	
	// Efficiently finds a person by first name. This operation should be O(1)
	Person findByFirstName(String name);
	
	// Efficiently finds a person by last name. This operation should be O(1)
	Person findByLastName(String name);
	
	// Finds all persons satisfying the given predicate
	List<Person> findAllBy(Predicate p);
	
	// Removes all persons satisfying the given predicate
	void removeAllBy(Predicate p);
	
	// Sorts all persons stored internally by lastName
	void sort();
	
	// Transforms this container into a list. Previous sortings should apply
	List<Person> toList();
}
