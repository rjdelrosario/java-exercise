package com.orchestronix.java.exercise;

public interface Queue {
	// Queues an element
	void queue(int i);
	
	// Dequeues an element
	int dequeue();
}
