package com.orchestronix.java.exercise;

public interface BubbleSort {
	// Sorts the given array and returns a new array with sorted elements.
	// Take note of the ascending parameter. 
	// If it is false, the elements should be sorted in descending order, e.g reverse
	int[] sort(int[] arr, boolean ascending);
}
