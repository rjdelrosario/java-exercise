package com.orchestronix.java.exercise;

public interface IntPredicate {
	// Returns boolean based on a custom comparison
	boolean eval(int i);
}
