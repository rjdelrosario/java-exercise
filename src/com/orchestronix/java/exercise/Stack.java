package com.orchestronix.java.exercise;

public interface Stack {
	// Clears all elements from this stack
	void clear();
	
	// Pushes an element into the stack
	void push(int i);
	
	// Pops an element out of the stack
	int pop();
}
