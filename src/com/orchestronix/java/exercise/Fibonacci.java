package com.orchestronix.java.exercise;

public interface Fibonacci {
	int fib(int n);
}
