package com.orchestronix.java.exercise;

public interface Predicate {
	// Returns boolean based on a custom comparison
	boolean eval(Person p);
}
