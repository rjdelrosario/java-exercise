package com.orchestronix.java.exercise;

public interface IntTransformer {
	// Transforms the given integer based a custom implementation
	int transform(int i);
}
